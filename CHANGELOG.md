# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.0] - 2020-2-16
### Added
- Redux-like archetecture overhaul: 
    - Divided state into appropriate substates 
    - Division of views into 'dumb' (only rendering) and 'smart' (business logic) components
    - Better code decoupling via map + component + container view structure
    - State normalization (digests, articles by id)
- Networking overhaul into ServerAPI
- Extracted some of the simple component logic into local state
- Personal card showing login suggestion in unauthorized state; personal articles reccomendation in authorized state
- Feedback template

## [0.4.0] - 2020-2-7
### Added
- Registration handling. Registration is done through a separate "For you" card

## [0.3.0] - 2020-2-5
### Added
- First draft of Redux-way architecture overhaul: Single direction data-flow: View -> Action -> Reducer -> State -> View. UI is derived from single state. Easy to debug and "free" routing, "report-a-bug" features

## [0.2.0] - 2020-2-2
### Added
- Main screen UI overhaul
- Model for server requests
- Model for digest playback

## [0.1.0] - 2019-11-11
### Added
- View modules for major screens: sign in, preferences and main (written with swift ui)

## [Unreleased]
- Routing
- "Report-a-bug"

[0.3.0]: https://gitlab.com/mayak-bigdata/news-app/-/tags/v0.2.0
[0.2.0]: https://gitlab.com/mayak-bigdata/news-app/-/tags/v0.2.0
[0.1.0]: https://gitlab.com/mayak-bigdata/news-app/-/tags/v0.1.0
[Unreleased]: https://gitlab.com/mayak-bigdata/news-app/compare/master...bdcc3e994aca30535f9f7a0948f2d276ff1d5bc5
