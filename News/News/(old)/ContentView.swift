//
//  ContentView.swift
//  News
//
//  Created by David Davitadze on 29.10.2019.
//  Copyright © 2019 David Davitadze. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
