////
////  PlaySound.swift
////  News
////
////  Created by David Davitadze on 26.01.2020.
////  Copyright © 2020 David Davitadze. All rights reserved.
////
//
//import Foundation
//import AVFoundation
//import Combine
//
//class DigestModel: ObservableObject {
//    private let willChange = PassthroughSubject<DigestModel, Never>()
//    private var audioPlayer: AVPlayer?
//    
//    var digests: [Digest]
//    var topics: [String] {
//        get {
//            var topics: [String] = []
//            for digest in self.digests {
//                topics.append(digest.topic)
//            }
//            return topics
//        }
//    }
//    
//    @Published var currentlyPlaying: Digest = nil
//    @Published var isPlaying: Bool = false {
//        willSet {
//            willChange.send(self)
//        }
//    }
//    
//    init(digests: [Digest]) {
//        self.digests = digests
//        
//        fetch()
//    }
//
//    func play(digest: Digest) {
//        if currentlyPlaying == nil || digest != currentlyPlaying {
//            if let url = URL(string: digest.audioUrl) {
//                let playerItem = AVPlayerItem(url: url)
//
//                self.audioPlayer = AVPlayer(playerItem: playerItem)
//                audioPlayer!.play()
//                
//                isPlaying = true
//                currentlyPlaying = digest
//            }
//        }
//        else if isPlaying {
//            audioPlayer!.pause()
//            isPlaying = false
//        }
//        else if !isPlaying {
//            audioPlayer!.play()
//            isPlaying = true
//        }
//    }
//    
//    func fetch() {
//        let backend_path = "http://35.244.212.175" + "/digests/list"
//        
//        getRequest(url: backend_path, headers: ["Content-Type": "application/json"]) { data in
//            if let data = data {
//                do {
//                    let json = try JSONSerialization.jsonObject(with: data, options: [])
//                    if let json = json as? [String] {
//                        for id in self.digests.indices {
//                            self.digests[id].topic = json[id]
//                        }
//                        print("fetch")
//                    }
//                } catch {
//                    print("JSON error: \(error.localizedDescription)")
//                }
//            }
//        }
//    }
//}
