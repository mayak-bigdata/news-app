//
//  Interest.swift
//  News
//
//  Created by David Davitadze on 01.11.2019.
//  Copyright © 2019 David Davitadze. All rights reserved.
//

import Foundation

import SwiftUI

struct Preference: Identifiable {
    var id = UUID()
    
    var name: String
}

#if DEBUG
let testPreferences = [
    Preference(name: "Social"),
    Preference(name: "Tech"),
    Preference(name: "Culture"),
    Preference(name: "Entertainment"),
    Preference(name: "Politics"),
    Preference(name: "Gossip"),
    Preference(name: "Science"),
    Preference(name: "Health"),
    Preference(name: "Sport"),
    Preference(name: "Economics"),
    Preference(name: "Auto"),
    Preference(name: "Urgent")
]
#endif
