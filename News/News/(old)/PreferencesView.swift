//
//  SimplifiedInterestsView.swift
//  News
//
//  Created by David Davitadze on 31.10.2019.
//  Copyright © 2019 David Davitadze. All rights reserved.
//

import SwiftUI

struct SimplifiedInterestsView: View {
    var body: some View {
        VStack {
            Text("Plese select topics you’re interested in. This will make our algorithm better and personalize digest for you!")
                .font(.title)
                .foregroundColor(Color.blue)
            
            Grid(rows: 4, columns: 3) { row, col in
                Button(action: {}) {
                    Image(systemName: "photo")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                }
            }
            
            Button(action: {}) {
                Text("Ok")
                    .font(.title)
            }
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding()
            .overlay(
                RoundedRectangle(cornerRadius: 5)
                    .stroke(lineWidth: 1)
                    .foregroundColor(Color.blue)
            )
        }
        .padding()
    }
}

struct SimplifiedInterestsView_Previews: PreviewProvider {
    static var previews: some View {
        SimplifiedInterestsView()
    }
}

struct Grid<Content: View>: View {
    let rows: Int
    let columns: Int
    let content: (Int, Int) -> Content

    var body: some View {
        VStack(spacing: 20) {
            ForEach(0 ..< rows) { row in
                HStack(spacing: 20) {
                    ForEach(0 ..< self.columns) { column in
                        self.content(row, column)
                    }
                }
            }
        }
    }

    init(rows: Int, columns: Int, @ViewBuilder content: @escaping (Int, Int) -> Content) {
        self.rows = rows
        self.columns = columns
        self.content = content
    }
}
