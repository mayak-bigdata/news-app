//
//  SignInView.swift
//  News
//
//  Created by David Davitadze on 31.10.2019.
//  Copyright © 2019 David Davitadze. All rights reserved.
//

import SwiftUI

struct SignInView: View {
    var socialNetworks: [SocialNetwork] = []
    
    var body: some View {
        VStack(spacing: 20) {
            Spacer()
                .layoutPriority(1)
            
            ForEach (socialNetworks) { socialNetwork in
                LoginOptionButton(
                    text:  socialNetwork.name,
                    imageName: socialNetwork.imageName)
            }
            LoginOptionButton(text: "Sign Up", imageName: nil)
            LoginOptionButton(text: "Sign In", imageName: nil)
            
            Spacer()
                .layoutPriority(1)
        }
        .padding()
    }
}

#if DEBUG
struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView(socialNetworks: testSocialNetworks)
    }
}
#endif

struct LoginOptionButton: View {
    let minSize: CGFloat? = 44
    
    let text: String
    let imageName: String?
    
    var body: some View {
        Button(action: {}) {
            imageName.map {
                Image($0)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            }
            Text("\(text)")
        }
        .frame(minWidth: minSize, maxWidth: .infinity,
               minHeight: minSize, maxHeight: .infinity)
        .padding()
        .overlay(
            RoundedRectangle(cornerRadius: 5)
                .stroke(lineWidth: 1)
                .foregroundColor(Color.blue)
        )
    }
}
