//
//  SocialNetwork.swift
//  News
//
//  Created by David Davitadze on 31.10.2019.
//  Copyright © 2019 David Davitadze. All rights reserved.
//

import SwiftUI

struct SocialNetwork: Identifiable {
    var id = UUID()
    var name: String
    
    var imageName: String {return name}
}

#if DEBUG
let testSocialNetworks = [
    SocialNetwork(name: "Google"),
    SocialNetwork(name: "Vkontakte")
]
#endif
