//
//  AppReducer.swift
//  News
//
//  Created by David Davitadze on 04.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

func appReducer(state: AppState, action: Action) -> AppState {
    var state = state
    
    state.digestsState = playButtonReducer(state: state.digestsState, action: action)
    state.digestsState = cardsReducer(state: state.digestsState, action: action)
    
    state.feedbackState = feedbackReducer(state: state.feedbackState, action: action)
    
    state.authState = loginReducer(state: state.authState, action: action)
    state = personalReducer(state: state, action: action)
    
    state = rootReducer(state: state, action: action)
    
    return state
}
