//
//  AppState.swift
//  News
//
//  Created by David Davitadze on 03.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

/// Combined app substates
struct AppState {
    var digestsState: DigestsState
    var authState: AuthState
    var feedbackState: FeedbackState
}

/// Represents fetched digests
/// and (optionally) which one user is currently listening to
///
/// Each digest could include isPlaying property but it is redundant
/// because at a given moment only one audio is playing
///
/// Also because of aforementioned reason isPlaying is
/// NOT a separate audio state. conceptually it belongs to digest collection
struct DigestsState {
    var digests: [String: Digest] = [:]
    var articles: [Int: Article] = [:]
    
    enum PlaybackState {
        case playing
        case paused
    }
    
    var currentlyPlayingDigest: (topic: String, playbackState: PlaybackState)? = nil
}

/// Represents user authentification state
/// and (optionally) fetched auth token
struct AuthState {
    var authToken: String? = ""
    var userId: Int? = -1
}


/// Represents user feedback state
///
/// Reactions leaved by user in form of {"article-header": [0, 1]} 0 - didn't like; 1 - liked
/// are stored in local state
struct FeedbackState {
    var isLeavingFeedbackForTopic: String? = nil
    var reactions: [Int: Bool] = [:]
}

