//
//  Architecture.swift
//  News
//
//  Created by David Davitadze on 01.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

// QUESTION: Why not typealias State = Any, Store = Any
//protocol StateType {}
//protocol StoreType {
//    associatedtype State: StateType
//
//    var state: State { get }
//    var dispatchFunction: DispatchFunction { get }
//
//    associatedtype ActionCreator = (_ state: State, _ store: StoreType) -> Action?
//    associatedtype AsyncActionCreator =
//    (_ state: State, _ store: StoreType,
//     _ actionCreatorCallback: (ActionCreator) -> Void) -> Void
//
//    func dispatch(_ actionCreator: ActionCreator)
//    func dispatch(_ asyncActionCreator: AsyncActionCreator)
//}
// QUESTION: Why not typealias State = AnyObject
// CLUE:     Maybe there is no convinient way of conforming to plain object type
protocol Action {}
//protocol AsyncAction: Action {}

// QUESTION: Why not typealias Reducer = (State, Action) -> State or
typealias Reducer<State> = (State, Action) -> State
//typealias DispatchFunction = (Action) -> Void
//typealias ActionCreator = (StateType, StoreType) -> Action


//typealias Middleware<State> = (@escaping DispatchFunction, @escaping () -> State?)
//    -> (@escaping DispatchFunction) -> DispatchFunction

final class Store<State>: ObservableObject {
    let objectWillChange = PassthroughSubject<State, Never>()
    
    @Published private(set) var state: State
    private let reducer: Reducer<State>
    private var cancellables: Set<AnyCancellable> = []

    init(state: State, reducer: @escaping (State, Action) -> State) {
        self.state = state
        self.reducer = reducer
    }

    func dispatch(_ action: Action) {
        self.state = self.reducer(self.state, action)
        objectWillChange.send(state)
        
        #if DEBUG
        print("--- State:")
        print(state)
        #endif
    }
    
    func dispatch(_ action: AnyPublisher<Action, Never>) {
        var cancellable: AnyCancellable?
        var didComplete = false
        
        cancellable = action
            .receive(on: DispatchQueue.main)
            .sink(
            receiveCompletion: { [weak self] _ in
                didComplete = true
                if let effectCancellable = cancellable {
                    self?.cancellables.remove(effectCancellable)
                }
            }, receiveValue: dispatch)
        
        if !didComplete, let actionCancellable = cancellable {
            cancellables.insert(actionCancellable)
        }
    }
}
