//
//  CardView.swift
//  News
//
//  Created by David Davitadze on 28.01.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import SwiftUI
import AVFoundation

let cardViewWidthMargin: CGFloat = 60
let cardViewHeightMargin: CGFloat = 195

// MARK: - Article View
struct ArticleView: View {
    @EnvironmentObject var store: Store<AppState>
    
    /// Typography constant
//    let headlineMargin: CGFloat = 10
    
    /// Props
    var article: Article

    var body: some View {
        VStack(alignment: .leading) {
            /// Should I change padding to both .top and .bottom?
            Text("\(self.article.header)")
            .fixedSize(horizontal: false, vertical: true)
            .font(.headline)
//          .padding(.bottom, headlineMargin)
            .padding(.bottom)

            Text("\(self.article.text)")
            .fixedSize(horizontal: false, vertical: true)
//            .frame(idealHeight: .greatestFiniteMagnitude)
            .font(.body)
            
        }
    }
}

// MARK: - Article Previewiew
struct ArticleView_Previews: PreviewProvider {
    static var previews: some View {
        ArticleView(article: testArticles[1]!)
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}

// MARK: - Article List View
struct ArticleListView: View {
    @EnvironmentObject var store: Store<AppState>
    
    /// Props
    var articles: [Article]
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading) {
                ForEach(self.articles) { article in
                    if (article.id != self.articles.last!.id) {
                        ArticleView(article: article)
                        .environmentObject(self.store)

                        Divider()
                        .padding()
                    }
                    else {
                        ArticleView(article: article)
                        .environmentObject(self.store)
                    }
                }
            }
        }
    }
}

// MARK: - Article List Preview
struct ArticleListView_Previews: PreviewProvider {
    @EnvironmentObject var store: Store<AppState>

    static var previews: some View {
        ArticleListView(articles: [testArticles[1]!, testArticles[2]!, testArticles[3]!])
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}

// MARK: - Play Button Component
/// Play button is implemented as a whole component (ie not separated in dumb view + smart container component)
/// because it is tightly bound to corresponding logic
struct PlayButtonComponent: View {
    @EnvironmentObject var store: Store<AppState>
    var topic: String
    
    /// Utility computed prop
    var digests: DigestsState {
        get {
            return store.state.digestsState
        }
    }
    
    /// Props
    var isPlaying: Bool {
        get {
            return digests.currentlyPlayingDigest == nil ?
            false : digests.currentlyPlayingDigest!.topic == topic
                && digests.currentlyPlayingDigest!.playbackState == .playing
        }
    }
    
    func pressPlayButton() {
        self.store.dispatch(
            fetchDigestUrl(forTopic: self.topic,
                           authToken: self.store.state.authState.authToken ?? "")
        )
    }
    
    func pressPauseButton() {
        self.store.dispatch(PlayButtonAction.pressedPause)
    }
    
    var body: some View {
        Button(action: {
            self.isPlaying ? self.pressPauseButton() : self.pressPlayButton()
        }) {
            Image(systemName: self.isPlaying ? "pause.circle" : "play.circle")
            .resizable()
            .aspectRatio(contentMode: .fit)
        }
        .frame(width: 44, height: 44)
    }
}

// MARK: - Play Button Preview
struct PlayButtonComponent_Previews: PreviewProvider {
    static var previews: some View {
        PlayButtonComponent(topic: testDigests["Политика"]!.topic)
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}

// MARK: - Card View
struct CardView: View {
    @EnvironmentObject var store: Store<AppState>
    
    /// UI Constants
//    let verticalMargin: CGFloat = 50
//    let titleMargin: CGFloat = 25

    /// Props
    var digest: Digest
    var articles: [Article]
    
    var body: some View {
        CardBorderOverlay {
            VStack(alignment: .leading) {
                HStack(alignment: .center) {
                    Text("\(self.digest.topic)")
                    .bold()
                    .font(.largeTitle)
//                    .font(.title)

                    Spacer()

                    PlayButtonComponent(topic: self.digest.topic)
//                    .padding(.trailing, 10)
                        .padding(.trailing)
                }
//                .padding(.bottom, self.titleMargin)
//                .padding(.top, self.verticalMargin)
                .padding(.vertical)

                ArticleListView(articles: self.articles)
//                .padding(.bottom, self.verticalMargin)
                
                Spacer()
            }
        }
    }
}

// MARK: - Card Container Preview
struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(digest: testDigests["Политика"]!,
                 articles: [testArticles[1]!, testArticles[2]!, testArticles[3]!])
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}
