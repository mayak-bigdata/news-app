//
//  PlayButtonAction.swift
//  News
//
//  Created by David Davitadze on 02.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

/// Represents possible actions user can perform on a given card
/// digest id is not needed when pause button is pressed - we know it's currently playing digest (only one possible at a time)
///
/// Conceptually we need to know only digest id - all other information is stored in DigestCollectionState (and hopefully can be retrieved)
enum PlayButtonAction: Action {
    case pressedPlay(topic: String, url: String)
    case pressedPause
}
