//
//  PlayButtonReducer.swift
//  News
//
//  Created by David Davitadze on 02.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

/// TODO implement case when audio ends but currently playing digest is not set to nil
/// TODO implement case when audio doesnt start playing but button gets pressed

import Foundation
import Combine

/// Manages cards collection related logic
/// i.e. audio playback
func playButtonReducer(state: DigestsState, action: Action) -> DigestsState {
    var state = state
    
    switch action {
    case let PlayButtonAction.pressedPlay(topic, url):
        #if DEBUG
        print("--- Pressed play")
        print(topic)
        #endif

        /// Play new audio digest
        /// if user pressed play button on another digest, switch playback
        if topic != state.currentlyPlayingDigest?.topic ?? "" {
            state.digests[topic]?.audioUrl = url
            AudioPlayer.sharedInstance.play(url: state.digests[topic]?.audioUrl ?? "")
            state.currentlyPlayingDigest = (topic, .playing)
        }
        /// Else user pressed play on the same digest, unpause
        else {
            AudioPlayer.sharedInstance.unpause()
            state.currentlyPlayingDigest!.playbackState = .playing
        }
        
    case PlayButtonAction.pressedPause:
        AudioPlayer.sharedInstance.pause()
        state.currentlyPlayingDigest!.playbackState = .paused
        
    default:
        break
    }
    
    return state
}

func fetchDigestUrl(forTopic topic: String, authToken: String) -> AnyPublisher<Action, Never> {
    print("--- Fetching digest summary")
    
    return ServerAPI.getDigestUrl(forTopic: topic, authToken: authToken)
    .replaceError(with: [:])
    .map { PlayButtonAction.pressedPlay(topic: topic, url: $0["url"]!) }
    .eraseToAnyPublisher()
}
