//
//  CardsAction.swift
//  News
//
//  Created by David Davitadze on 04.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

enum CardsAction: Action {
    case fetchedTopics(topics: [String])
    case fetchedDigestSummary(digestSummary: DigestSummary)
}
