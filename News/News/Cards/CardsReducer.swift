//
//  CardsReducer.swift
//  News
//
//  Created by David Davitadze on 04.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import Combine

extension ArticleSummary {
    func toArticle() -> Article {
        return Article(
            id: self.id,
            header: self.name,
            text: self.description,
            url: ""
        )
    }
}

func cardsReducer(state: DigestsState, action: Action) -> DigestsState {
    var state = state
    
    switch action {
//    case CardsAction.fetchedTopics(let topics):
//        for (topic, id) in zip(topics, Array(state.digests.keys)) {
//            state.digests[id]!.topic = topic
//        }
    
    case CardsAction.fetchedDigestSummary(let digestSummary):
        if digestSummary.status == "success" {
            
            state.digests[digestSummary.name]!.articles = []
            
            for article in digestSummary.contents.map({ $0.toArticle() }) {
                state.digests[digestSummary.name]!.articles.append(article.id)
                state.articles[article.id] = article
            }
        }
        
    default:
        break
    }
    
    return state
}

func fetchTopics() -> AnyPublisher<Action, Never> {
//    let backend_path = "http://35.244.212.175" + "/digests/list"
//
//    do {
//        return try Networking().get(url: backend_path, headers: ["Content-Type": "application/json"])
//            .replaceError(with: [])
//            .map { CardsAction.fetchedTopics(digests: $0) }
//            .eraseToAnyPublisher()
//    } catch {
//        return Empty<Action, Never>()
//            .eraseToAnyPublisher()
//    }
    
    return ServerAPI.getTopicsList()
    .replaceError(with: [])
    .map { CardsAction.fetchedTopics(topics: $0) }
    .eraseToAnyPublisher()
}

func fetchDigestSummary(forTopic topic: String, authToken: String) -> AnyPublisher<Action, Never> {
    print("--- Fetching digest summary")
    
    return ServerAPI.getDigestSummary(forTopic: topic, authToken: authToken)
    .replaceError(with: DigestSummary())
    .map { CardsAction.fetchedDigestSummary(digestSummary: $0) }
    .eraseToAnyPublisher()
}
