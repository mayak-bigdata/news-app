//
//  CardsView.swift
//  News
//
//  Created by David Davitadze on 31.10.2019.
//  Copyright © 2019 David Davitadze. All rights reserved.
//

import SwiftUI

// MARK: - Cards View
struct CardsView: View {
    @EnvironmentObject var store: Store<AppState>
    
    /// UI Constants
    let spacing: CGFloat = 30
    
    /// Props
    struct Props {
        var digests: [Digest]
        var articles: [Int: Article]
        
        var fetchTopics: () -> Void
        var fetchDigest: (_ withTopic: String) -> Void
        
        func getArticles(for digest: Digest) -> [Article] {
            var articles: [Article] = []
            for id in digest.articles {
                articles.append(self.articles[id]!)
            }
            
            return articles
        }
    }
    var props: Props
    
    var body: some View {
        GeometryReader { geometry in
            DraggableScrollviewOverlay(numberOfPages: self.props.digests.count + 1, spacing: self.spacing) {
                HStack(alignment: .center, spacing: self.spacing) {
                    /// For You card
                    
                    GeometryReader { card in
                        PersonalCardView()
                        .padding()
                        .rotation3DEffect(
                            Angle(degrees: Double(card.frame(in: .global).minX) / -20),
                            axis: (x: 0, y: 10.0, z: 0)
                        )
                    }
                    .frame(width: geometry.size.width, height: geometry.size.height)
                    
                    /// Digest cards
                    
                    ForEach(self.props.digests) { digest in
                        GeometryReader { card in
                            CardView(digest: digest,
                                     articles: self.props.getArticles(for: digest))
                            .padding()
                            .rotation3DEffect(
                                Angle(degrees: Double(card.frame(in: .global).minX) / -20),
                                axis: (x: 0, y: 10.0, z: 0)
                            )
                        }
                        .frame(width: geometry.size.width, height: geometry.size.height)
                    }
                }
            }
        }
        .onAppear {
//            self.props.fetchTopics()
            for digest in self.props.digests {
                print(digest.topic)
                self.props.fetchDigest(digest.topic)
            }
        }
    }
}

// MARK: - Map Store to Cards
func mapStoreToCardsView(store: Store<AppState>) -> CardsView.Props {
    return CardsView.Props(
        digests: Array(store.state.digestsState.digests.values)
            .sorted{ $0.topic < $1.topic }
            .filter{ $0.topic != "Personal" },
        articles: store.state.digestsState.articles,
        fetchTopics: { store.dispatch(fetchTopics()) },
        fetchDigest: { topic in store.dispatch(fetchDigestSummary(forTopic: topic, authToken: store.state.authState.authToken ?? "")) }
    )
}

// MARK: - Cards Container View
struct CardsContainerView: View {
    @EnvironmentObject var store: Store<AppState>
    let map: (Store<AppState>) -> CardsView.Props
    
    var body: some View {
        CardsView(props: self.map(self.store))
    }
}

// MARK: - Cards Container View Preview
struct CardsView_Previews: PreviewProvider {
    static var previews: some View {
        CardsContainerView(
            map: mapStoreToCardsView
        )
        .environmentObject(appStore)
    }
}

//  @State var showFeedBackScreen: Bool
    
//   Maps state onto concrete view props
//    func map(state: AppState) -> Props {
//        return Props(
//            digests: state.digests,
//            showFeedBackScreen: state.isGivingFeedback
//        )
//    }

//    struct ActionCreators {
//        // Action creators
//        let fetchDigests: ActionCreator
//    }
    
//   Bind function
//    func bind(bind: Bind) -> ActionCreators {
//        return ActionCreators(
//            fetchDigests: bind(fetchDigests())
//        )
//    }

//                    GeometryReader { card in
//                        UnauthorizedView(store: appStore)
//                            .rotation3DEffect(
//                                Angle(degrees: Double(card.frame(in: .global).minX) / -20),
//                                axis: (x: 0, y: 10.0, z: 0)
//                            )
//                    }
//                    .frame(width: geometry.size.width)

//        .sheet(isPresented: props.$showFeedBackScreen) {
//            FeedbackView(store: self.store, digest: self.store.appState.currentDigest!)
//        }
