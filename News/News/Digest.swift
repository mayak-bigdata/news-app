//
//  Digest.swift
//  News
//
//  Created by David Davitadze on 08.11.2019.
//  Copyright © 2019 David Davitadze. All rights reserved.
//

import Foundation

class IntIdGenerator {
    private var nextId = 0

    func generate() -> Int {
        nextId += 1
        return nextId
    }
}


struct Article: Identifiable {
//    private static let idGenerator = IntIdGenerator()

    init(id: Int, header: String, text: String, url: String) {
//        id = Article.idGenerator.generate()
        self.id = id
        self.header = header
        self.text = text
        self.url = url
    }
    
    var id: Int = -1
    var header: String = ""
    var text: String = ""
    var url: String = ""
}

struct Digest: Identifiable, Equatable {
    private static let idGenerator = IntIdGenerator()
    let id: Int

    init(topic: String, articles: [Int], audioUrl: String) {
        id = Digest.idGenerator.generate()
        
        self.topic = topic
        self.articles = articles
        self.audioUrl = audioUrl
    }
    
    var topic: String = ""
    var articles: [Int] = []
    var audioUrl: String = ""
    
    static func ==(lhs: Digest, rhs: Digest) -> Bool {
        return lhs.id == rhs.id
    }
}

#if DEBUG
let testDigests = [
    "Политика": Digest(
        topic: "Политика",
        articles: [1, 2, 3],
        audioUrl: "http://35.244.212.175/digests/hls/Политика/index.m3u8"
    ),
    
    "Экономика": Digest(
        topic: "Экономика",
        articles: [4, 5, 6],
        audioUrl: "http://35.244.212.175/digests/hls/Экономика/index.m3u8"
    ),
    
    "Культура": Digest(
        topic: "Культура",
        articles: [7, 8, 9],
        audioUrl: "http://35.244.212.175/digests/hls/Культура/index.m3u8"
    ),
    
    "Personal": Digest(
        topic: "Personal",
        articles: [1, 4, 7],
        audioUrl: "http://35.244.212.175/digests/hls/1/index.m3u8"
    )
]

let testArticles = [
    1: Article(id: 1, header: "Epstein inquiry", text: "FBI claims Duke of York has failed to cooperate", url: "http://bit.ly/2GuuDti"),
    2: Article(id: 2, header: "The allegations", text: "How Prince Andrew's version of events compare", url: "http://bit.ly/2TXFOCC"),
    3: Article(id: 3, header: "Smart motorways", text: "Road chiefs face criminal claims over deaths", url: "http://bit.ly/2RW3PHC"),
    
    4: Article(id: 4, header: "New 50p", text: "A very British stir over how to phrase a coin", url: "http://bit.ly/2TXFVy2"),
    5: Article(id: 5, header: "Huawei announcement", text: "PM set to allow firm into 5G network", url: "http://bit.ly/2RAjOMC"),
    6: Article(id: 6, header: "Coronavirus latest", text: "Death toll passes 100 - all you need to know", url: "http://bit.ly/2t8TbF6"),
    
    7: Article(id: 7, header: "Prince Andrew", text:  "Duke of York 'did not respond' to FBI requests for interview about Epstein, reports claim", url: "http://bit.ly/2TZTrkL"),
    8: Article(id: 8, header: "Coronavirus in pictures", text: "Images as China races to build specialist hospitals", url: "http://bit.ly/2vslvmz"),
    9: Article(id: 9, header: "Bryant helicopter crash", text: "Investigation into tragedy that killed basketball player", url: "http://bit.ly/36zjPEC")
]
#endif
