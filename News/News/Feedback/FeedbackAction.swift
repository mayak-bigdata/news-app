//
//  FeedbackAction.swift
//  News
//
//  Created by David Davitadze on 08.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

enum FeedbackAction: Action {
    case pressedLoveButton(id: Int)
    case sentFeedback(reply: [String: Any])
}
