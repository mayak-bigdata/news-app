//
//  FeedbackReducer.swift
//  News
//
//  Created by David Davitadze on 08.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import Combine

func feedbackReducer(state: FeedbackState, action: Action) -> FeedbackState {
    var state = state
    
    switch action {
    case FeedbackAction.pressedLoveButton(let id):
        if let _ = state.reactions[id] {
            state.reactions[id]!.toggle()
        } else {
            state.reactions[id] = true
        }
        
    case FeedbackAction.sentFeedback(let reply):
        #if DEBUG
        print("--- Got reply")
        print(reply)
        #endif
        state.isLeavingFeedbackForTopic = nil
        state.reactions = [:]
        
    default:
        break
    }
    
    return state
}

func sendFeedback(store: Store<AppState>) -> AnyPublisher<Action, Never> {
    return ServerAPI.sendFeedback(
        reactions: ["reactions":
            Dictionary(uniqueKeysWithValues: store.state.feedbackState.reactions.map{ key, value in (String(key), Int(truncating: value as NSNumber)) })
        ],
        authToken: store.state.authState.authToken ?? ""
    )
    .replaceError(with: [:])
    .map { FeedbackAction.sentFeedback(reply: $0) }
    .eraseToAnyPublisher()
}
