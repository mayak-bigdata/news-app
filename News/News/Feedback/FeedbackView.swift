//
//  FeedbackView.swift
//  News
//
//  Created by David Davitadze on 08.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import SwiftUI

// MARK: - Headline View
struct HeadlineView: View {
    @EnvironmentObject var store: Store<AppState>
    
    var id: Int
    
    var headline: String {
        get {
            return store.state.digestsState.articles[self.id]?.header ?? ""
        }
    }
    
    var loved: Bool {
        get {
            return store.state.feedbackState.reactions[self.id] ?? false
        }
    }
    
    func pressLoveButton() {
        self.store.dispatch(FeedbackAction.pressedLoveButton(id: self.id))
    }
    
    var body: some View {
        HStack(alignment: .center) {
            Text("\(headline)")
            .font(.headline)

            Spacer()

            Button(action: {
                self.pressLoveButton()
            }) {
                Image(systemName: self.loved ? "heart.fill" : "heart")
                .resizable()
                .aspectRatio(contentMode: .fit)
            }
            .frame(width: 44, height: 44)
//            .padding(.trailing, 10)
            .padding(.trailing)
        }
    }
}

// MARK: - Headline Preview
struct HeadlineView_Previews: PreviewProvider {
    static var previews: some View {
        HeadlineView(id: 4)
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)

    }
}

// MARK: - Headline Stack View
struct HeadlineStackView: View {
    @EnvironmentObject var store: Store<AppState>
    
    struct Props {
        var articles: [Article]
    }
    
    var props: Props

    var body: some View {
        VStack(alignment: .leading) {
            ForEach(self.props.articles) { article in
                if (article.id != self.props.articles.last!.id) {
                    HeadlineView(id: article.id)

                    Divider()
                    .padding()
                }
                else {
                    HeadlineView(id: article.id)
                }
            }
        }
    }
}

// MARK: - Map Store to Headline Stack
func mapStoreToHeadlineStackView(store: Store<AppState>) -> HeadlineStackView.Props {
    let digests = store.state.digestsState.digests
    let isLeavingFeedbackForTopic = store.state.feedbackState.isLeavingFeedbackForTopic ?? ""
    let digest = digests[isLeavingFeedbackForTopic] ?? Digest(topic: "", articles: [], audioUrl: "")
    
    var articles: [Article] = []
    for id in digest.articles {
        articles.append(store.state.digestsState.articles[id]!)
    }
    
    return HeadlineStackView.Props(
        articles: articles
    )
}

// MARK: - Headline Stack Container View
struct HeadlineStackContainerView: View {
    @EnvironmentObject var store: Store<AppState>
    let map: (Store<AppState>) -> HeadlineStackView.Props
    
    var body: some View {
        HeadlineStackView(props: self.map(self.store))
    }
}

// MARK: - Headline Stack Container Preview
struct HeadlineStackContainerView_Previews: PreviewProvider {
    static var previews: some View {
        HeadlineStackContainerView(map: mapStoreToHeadlineStackView)
        .environmentObject(
            appStore
        )
        .previewLayout(.sizeThatFits)
    }
}

// MARK: - Feedback View
struct FeedbackView: View {
    @EnvironmentObject var store: Store<AppState>

//    let verticalMargin: CGFloat = 50
//    let titleMargin: CGFloat = 25

    var body: some View {
        return VStack(alignment: .leading) {
            Text("Please leave feedback!")
            .bold()
            .font(.largeTitle)
            .fixedSize(horizontal: false, vertical: true)
            .padding(.vertical)
            
            ScrollView(.vertical, showsIndicators: false) {
                HeadlineStackContainerView(map: mapStoreToHeadlineStackView)
            }

            Button("Send Feedback") {
                self.store.dispatch(sendFeedback(store: self.store))
            }
            .font(.headline)
            .foregroundColor(.white)
            .frame(maxWidth: .infinity)
            .padding()
            .background(Color.blue)
            .cornerRadius(12)
            .padding(.vertical)
        }
        .padding()
    }
}

// MARK: - Feedback View Preview
struct FeedbackView_Previews: PreviewProvider {
    static var previews: some View {
        FeedbackView()
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}
