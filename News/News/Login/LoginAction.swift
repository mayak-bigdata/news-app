//
//  LoginAction.swift
//  News
//
//  Created by David Davitadze on 05.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

enum LoginAction: Action {
    case fetchedAuthToken(authToken: String)
}
