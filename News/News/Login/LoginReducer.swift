//
//  LoginReducer.swift
//  News
//
//  Created by David Davitadze on 05.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import Combine

func loginReducer(state: AuthState, action: Action) -> AuthState {
    var state = state
    
    switch action {
    case LoginAction.fetchedAuthToken(let authToken):
        #if DEBUG
        print("--- Fetched auth token:")
        print(authToken)
        #endif
        
        state.authToken = authToken
    
    default:
        break
    }
    
    return state
}

func registerUser(email: String, password: String) -> AnyPublisher<Action, Never> {
    return ServerAPI.registerUser(email: email, password: password)
        .replaceError(with: [:])
        .map { LoginAction.fetchedAuthToken(authToken: $0["auth_token"]!) }
        .eraseToAnyPublisher()
}

func loginUser(email: String, password: String) -> AnyPublisher<Action, Never> {
    return ServerAPI.loginUser(email: email, password: password)
        .replaceError(with: [:])
        .map { LoginAction.fetchedAuthToken(authToken: $0["auth_token"]!) }
        .eraseToAnyPublisher()
}

