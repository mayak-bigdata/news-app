//
//  LoginView.swift
//  News
//
//  Created by David Davitadze on 05.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import SwiftUI

// MARK: - TextField Overlay

struct TextFieldOverlay<V: View>: View {
    let contents: () -> V

    init(contents: @escaping () -> V) {
        self.contents = contents
    }

    var body: some View {
        self.contents()
        .font(.headline)
        .padding()
        .overlay(
            RoundedRectangle(cornerRadius: 12)
                .stroke(Color.gray, lineWidth: 1)
        )
    }
}

// MARK: - LoginButton Overlay

struct LoginButtonOverlay<V: View>: View {
    let contents: () -> V

    init(contents: @escaping () -> V) {
        self.contents = contents
    }

    var body: some View {
        self.contents()
        .font(.headline)
        .foregroundColor(.white)
        .frame(maxWidth: .infinity)
        .padding()
        .background(Color.blue)
        .cornerRadius(12)
    }
}

// MARK: - Login View

struct LoginView: View {
    @EnvironmentObject var store: Store<AppState>
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    // Constants
//    let titleMargin: CGFloat = 15
//    let headlineMargin: CGFloat = 0
//    let verticalMargin: CGFloat = 50
    
    // Local state
    @State private var email = "user@gmail.com"
    @State private var password = "123456"
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            /// Header
            
            Text("Account")
            .bold()
            .font(.largeTitle)
//            .font(.title)
//            .padding(.top, verticalMargin)
//            .padding(.bottom, titleMargin)
            .padding(.vertical)

            
            /// Input text fields
            
            TextFieldOverlay {
                TextField("Email", text: self.$email)
                .keyboardType(.emailAddress)
            }
            
            TextFieldOverlay {
                TextField("Password", text: self.$password)
            }

            /// Login buttons
            
            Button(action: {
                self.store.dispatch(
                    registerUser(email: self.email, password: self.password)
                )
            }) {
                LoginButtonOverlay {
                    Text("Sign Up")
                }
            }
            
            Button(action: {
                self.store.dispatch(
                    loginUser(email: self.email, password: self.password)
                )
            }) {
                LoginButtonOverlay {
                    Text("Sign In")
                }
            }

            Spacer()
        }
        .padding()
    }
}

// MARK: - Login Preview

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}

//    struct Props { }
//
//    // Maps state onto concrete view props
//    func map(state: AppState) -> Props { return Props() }
//
//    struct ActionCreators {
//        // Action creators
//        let registerUser: ActionCreator
//        let loginUser: ActionCreator
//    }
//
//    // Bind function
//    func bind(bind: Bind) -> ActionCreators {
//        return ActionCreators(
//            registerUser: bind(registerUser(email: self.email, password: self.password)),
//            loginUser: bind(loginUser(email: self.email, password: self.password))
//        )
//    }
