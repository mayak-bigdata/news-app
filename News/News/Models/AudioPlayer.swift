//
//  AudioPlayer.swift
//  News
//
//  Created by David Davitadze on 04.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import AVFoundation

class AudioPlayer {
    var audioPlayer = AVPlayer()

    static let sharedInstance: AudioPlayer = { AudioPlayer() }()

    func play(url: String){
        print(url)
        
        if let url = URL(string: url.encodeUrl) {
            let playerItem = AVPlayerItem(url: url)
            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
            
            audioPlayer.replaceCurrentItem(with: playerItem)
            audioPlayer.play()
            
//            audioPlayer.seek(to: CMTimeMake(value: 60, timescale: 1))
        }
    }

    @objc func playerDidFinishPlaying(sender: Notification) {
        appStore.dispatch(RootAction.playerFinishedPlaying)
    }

    func pause(){
        audioPlayer.pause()
    }

    func unpause() {
        audioPlayer.play()
    }
}

extension AudioPlayer {
    var isPlaying: Bool {
        return audioPlayer.timeControlStatus == .playing
    }

    var currentlyPlayingItem: String? {
        return (audioPlayer.currentItem?.asset as? AVURLAsset)?.url.absoluteString
    }
}
