//
//  Networking.swift
//  News
//
//  Created by David Davitadze on 27.01.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import Combine

// TODO: Refactor in a python requests library style

enum NetworkingError: Error {
    case invalidServerResponse
    case invalidUrlString
    case invalidDataFormat
}

struct Networking {
    private func request(for request: URLRequest) -> AnyPublisher<Data, Error> {
        return URLSession.shared.dataTaskPublisher(for: request)
        .tryMap { data, response -> Data in
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    #if DEBUG
                    print("--- Invalid server response:")
                    print(response)
                    #endif
                    
                    throw NetworkingError.invalidServerResponse
            }
            
            #if DEBUG
            print("--- Got data in response")
            do {
                print("--- Data:")
                print(try JSONSerialization.jsonObject(with: data, options: []) as Any)
            } catch {
                print("--- Couldn't decode")
            }
            #endif
            
            return data
        }
        .eraseToAnyPublisher()
    }
    
    private func decode<T: Decodable>(request: AnyPublisher<Data, Error>) -> AnyPublisher<T, Error> {
        return request
        .tryMap { data -> T in
            guard let decodedData = try? JSONDecoder().decode(T.self, from: data) else {
                #if DEBUG
                print("--- Couldn't decode data")
                #endif
                
                throw NetworkingError.invalidDataFormat
            }
            
            #if DEBUG
            print("--- Decoded data:")
            print(decodedData)
            #endif
            
            return decodedData
        }
        .eraseToAnyPublisher()
    }
    
    private func toUrl(_ string: String) throws -> URL {
        guard let url = URL(string: string.encodeUrl) else {
            #if DEBUG
            print("Couldn't construct url. Invalid string")
            #endif
            
            throw NetworkingError.invalidUrlString
        }
        
        #if DEBUG
        print("--- Constructed url:")
        print(url)
        #endif
        
        return url
    }
    
    private func configurePostRequest<H: Hashable>(url: URL, headers: [String: String], data: [H : Any]) throws -> URLRequest {
        #if DEBUG
        print("--- Configuring POST request")
        print("url: \(url)")
        print("headers: \(headers)")
        print("data: \(data)")
        #endif
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        for header in headers {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }
        
        guard let json = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted) else {
            #if DEBUG
            print("Couldn't encode data")
            #endif
            
            throw NetworkingError.invalidDataFormat
        }
        request.httpBody = json
        
        return request
    }
    
    func get<T: Decodable>(url: String, headers: [String: String]) throws -> AnyPublisher<T, Error>{
        #if DEBUG
        print("--- GET request")
        print("url: \(url)")
        print("headers: \(headers)")
        #endif
        
        let url = try toUrl(url)
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        for header in headers {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }
        
        return self.decode(request: self.request(for: request))
    }

    func post<T: Decodable, H: Hashable>(url: String, headers: [String: String], data: [H : Any]) throws -> AnyPublisher<T, Error> {
        #if DEBUG
        print("--- POST request")
        #endif
        
        let url = try toUrl(url)
        let postRequest = try configurePostRequest(url: url, headers: headers, data: data)
        
        return self.decode(request: self.request(for: postRequest))
    }
}
