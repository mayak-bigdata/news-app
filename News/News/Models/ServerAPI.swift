//
//  ServerAPI.swift
//  News
//
//  Created by David Davitadze on 09.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import Combine

/// Represets article as provided by server
struct ArticleSummary: Decodable {
    var name: String = ""
    var description: String = ""
    var id: Int = 0
    var file_name: String = ""
}

/// Represets digest as provided by server
struct DigestSummary: Decodable {
    var status: String = ""
    var name: String = ""
    var contents: [ArticleSummary] = []
}

struct UserData: Decodable {
    var email: String = ""
    var registered_on: String = ""
    var user_id: Int = Int()
}

struct UserInfo: Decodable {
    var status: String = ""
    var data: UserData = UserData()
}

struct Reccomendations: Decodable {
    var status: String = ""
    var user_id: Int = Int()
    var rec: [Int] = []
}

enum ServerAPI {
    static let agent = Networking()
    static let backendPath = "http://35.244.212.175"
    
    static func registerUser(email: String, password: String) -> AnyPublisher<[String:String], Never> {
        let url = backendPath + "/registration/register"
        
        let headers = [
            "Content-Type": "application/json",
        ]
        
        let data : [String : Any] = [
            "email": email,
            "password": password,
            "preferences": ""
        ]
        
        do {
            return try agent.post(url: url, headers: headers, data: data)
            .replaceError(with: [:])
            .eraseToAnyPublisher()
        } catch {
            return Just([:]).eraseToAnyPublisher()
        }
    }
    
    static func loginUser(email: String, password: String) -> AnyPublisher<[String:String], Never> {
        let url = backendPath + "/registration/login"
        
        let headers = [
            "Content-Type": "application/json",
        ]
        
        let data : [String : Any] = [
            "email": email,
            "password": password
        ]
        
        do {
            return try agent.post(url: url, headers: headers, data: data)
            .replaceError(with: [:])
            .eraseToAnyPublisher()
        } catch {
            return Just([:]).eraseToAnyPublisher()
        }
    }
    
    static func getUserInfo(authToken: String) -> AnyPublisher<UserInfo, Never> {
            print("--- Getting user info")
            
            let url = backendPath + "/registration/info"
                
            let headers = [
                "Content-Type": "application/json",
                "Authorization": "Bearer " + authToken
            ]
            
            do {
                return try Networking().get(url: url, headers: headers)
                .replaceError(with: UserInfo())
                .eraseToAnyPublisher()
            } catch {
                return Just(UserInfo()).eraseToAnyPublisher()
            }
        }
    
    static func getTopicsList() -> AnyPublisher<[String], Never> {
        let url = backendPath + "/digests/list"
                
        let headers = [
            "Content-Type": "application/json"
        ]
        
        do {
            return try agent.get(url: url, headers: headers)
            .replaceError(with: [])
            .eraseToAnyPublisher()
        } catch {
            print("error")
            return Just([]).eraseToAnyPublisher()
        }
    }
    
    static func getDigestSummary(forTopic topic: String, authToken: String) -> AnyPublisher<DigestSummary, Never> {
        print("--- Getting digest summary")
        
        let url = backendPath + "/digests/summary/"
            
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer " + authToken
        ]
        
        do {
            return try Networking().get(url: url + topic, headers: headers)
            .replaceError(with: DigestSummary())
            .eraseToAnyPublisher()
        } catch {
//            print("couldn't get digest summary \(url + topic)")
            return Just(DigestSummary()).eraseToAnyPublisher()
        }
    }
    
    static func getDigestUrl(forTopic topic: String, authToken: String) -> AnyPublisher<[String: String], Never> {
            print("--- Getting digest url")
            
            let url = backendPath + "/digests/get/"
                
            let headers = [
                "Content-Type": "application/json",
                "Authorization": "Bearer " + authToken
            ]
            
            do {
                return try Networking().get(url: url + topic, headers: headers)
                .replaceError(with: [:])
                .eraseToAnyPublisher()
            } catch {
    //            print("couldn't get digest summary \(url + topic)")
                return Just([:]).eraseToAnyPublisher()
            }
        }
    
    static func sendFeedback(reactions: [String: [String: Int]], authToken: String) -> AnyPublisher<[String:[String: Int]], Never> {
        let url = backendPath + "/recommender/feedback"
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer " + authToken
        ]
        
        print(reactions)
        
        do {
            return try Networking().post(url: url, headers: headers, data: reactions)
            .replaceError(with: [:])
            .eraseToAnyPublisher()
        } catch {
            return Just([:]).eraseToAnyPublisher()
        }
    }
    
    static func getReccomendations(authToken: String) -> AnyPublisher<Reccomendations, Never> {
        print("--- Getting reccomendations")
        
        let url = backendPath + "/recommender/recommend"
            
        let headers = [
            "Content-Type": "application/json",
            "Authorization": "Bearer " + authToken
        ]
        
        do {
            return try Networking().get(url: url, headers: headers)
            .replaceError(with: Reccomendations())
            .eraseToAnyPublisher()
        } catch {
//            print("couldn't get digest summary \(url + topic)")
            return Just(Reccomendations()).eraseToAnyPublisher()
        }
    }
}
