//
//  PersonalAction.swift
//  News
//
//  Created by David Davitadze on 16.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

enum PersonalAction: Action {
    case fetchedUserInfo(userInfo: UserInfo)
    case fetchedReccomendations(reccomendations: Reccomendations)
//    case fetchedUrl(url: String)
}
