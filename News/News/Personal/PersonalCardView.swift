//
//  PersonalCardView.swift
//  News
//
//  Created by David Davitadze on 16.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import SwiftUI

struct PersonalCardView: View {
    @EnvironmentObject var store: Store<AppState>
    
    var authorized: Bool {
        get {
            return store.state.authState.authToken != nil
        }
    }
    
    var body: some View {
        if self.authorized == false {
            return AnyView(
                UnauthorizedView()
                .environmentObject(self.store)
            )
        }
        else {
//            let personalDigest = Digest(
//                topic: "Personal",
//                articles: [1, 4, 7],
//                audioUrl: "http://35.244.212.175/digests/hls/\(self.store.state.authState.userId ?? 1)/index.m3u8"
//            )
            
            let pesonalArticles: [Article] = {
                var articles: [Article] = []
                
                for id in self.store.state.digestsState.digests["Personal"]!.articles {
                    articles.append(
                        self.store.state.digestsState.articles[id]!
                    )
                }
                
                return articles
            }()
            
            return AnyView(
                CardView(
                    digest: self.store.state.digestsState.digests["Personal"]!,
                    articles: pesonalArticles
                )
                .environmentObject(self.store)
                .onAppear {
                    self.store.dispatch(
                        fetchReccomendations(
                            authToken: self.store.state.authState.authToken!
                        )
                    )
//                    self.store.dispatch(
//                        fetchReccomendations(
//                            authToken: self.store.state.authState.authToken!
//                        )
//                    )
                }
            )
        }
    }
}

struct PersonalCardView_Previews: PreviewProvider {
    static var previews: some View {
        PersonalCardView()
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}

//func mapStoreToPersonalView(store: Store<AppState>) -> PersonalCardView.Props {
//    return PersonalCardView.Props(
//        authorized: store.state.authState.authToken != nil
//    )
//}
//
//struct PersonalCardContainerView: View {
//    @EnvironmentObject var store: Store<AppState>
//    let map: (Store<AppState>) -> PersonalCardView.Props
//
//    var body: some View {
//        PersonalCardView(props: self.map(self.store))
//    }
//}

        
