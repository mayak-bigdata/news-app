//
//  PersonalReducer.swift
//  News
//
//  Created by David Davitadze on 16.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation
import Combine

func personalReducer(state: AppState, action: Action) -> AppState {
    var state = state
    
    switch action {
    case PersonalAction.fetchedUserInfo(let userInfo):
        #if DEBUG
        print("--- Fetched user info:")
        print(userInfo)
        #endif
        
        if userInfo.status == "success" {
            state.authState.userId = userInfo.data.user_id
            state.digestsState.digests["Personal"]?.audioUrl = "http://35.244.212.175/digests/hls/\(userInfo.data.user_id)/index.m3u8"
        }
    
    case PersonalAction.fetchedReccomendations(let reccomendations):
        if reccomendations.status == "success" {
            state.authState.userId = reccomendations.user_id
            
            state.digestsState.digests["Personal"]?.audioUrl = "http://35.244.212.175/digests/hls/\(reccomendations.user_id)/index.m3u8"
            state.digestsState.digests["Personal"]?.articles = reccomendations.rec
        }
        
    default:
        break
    }
    
    return state
}

func fetchUserInfo(authToken: String) -> AnyPublisher<Action, Never> {
    #if DEBUG
    print("--- Fetching user info")
    #endif
    
    return ServerAPI.getUserInfo(authToken: authToken)
        .replaceError(with: UserInfo())
        .map { PersonalAction.fetchedUserInfo(userInfo: $0) }
        .eraseToAnyPublisher()
}

func fetchReccomendations(authToken: String) -> AnyPublisher<Action, Never> {
    #if DEBUG
    print("--- Fetching reccomendations")
    #endif
    
    return ServerAPI.getReccomendations(authToken: authToken)
        .replaceError(with: Reccomendations())
        .map { PersonalAction.fetchedReccomendations(reccomendations: $0) }
        .eraseToAnyPublisher()
}

//func fetchPersonalUrl(authToken: String) -> AnyPublisher<Action, Never> {
//    print("--- Fetching digest summary")
//
//    return ServerAPI.getDigestUrl(forTopic: "Personal", authToken: authToken)
//    .replaceError(with: [:])
//    .map { PlayButtonAction.pressedPlay(url: $0["url"]!) }
//    .eraseToAnyPublisher()
//}
