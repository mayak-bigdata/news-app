//
//  RootAction.swift
//  News
//
//  Created by David Davitadze on 17.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

enum RootAction: Action {
    case playerFinishedPlaying
}
