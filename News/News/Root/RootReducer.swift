//
//  RootReducer.swift
//  News
//
//  Created by David Davitadze on 17.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

func rootReducer(state: AppState, action: Action) -> AppState {
    var state = state
    
    switch action {
        case RootAction.playerFinishedPlaying:
            state.feedbackState.isLeavingFeedbackForTopic
                = state.digestsState.currentlyPlayingDigest!.topic
            state.digestsState.currentlyPlayingDigest = nil
        
        default:
         break
    }

    return state
}
