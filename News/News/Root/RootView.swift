//
//  RootView.swift
//  News
//
//  Created by David Davitadze on 17.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import SwiftUI

struct RootView: View {
    @EnvironmentObject var store: Store<AppState>
    @State var showFeedback: Bool = false
        
    
    var body: some View {
        let showFeedbackBinding = Binding<Bool>(
            get: {
                self.store.state.feedbackState.isLeavingFeedbackForTopic != nil &&
                    self.store.state.authState.authToken != nil
            },
            set: {
                self.showFeedback = $0
            }
        )
        
        return VStack {
            CardsContainerView(
                map: mapStoreToCardsView
            )
            
            Text("")
            .sheet(isPresented: showFeedbackBinding) {
                FeedbackView()
                .environmentObject(appStore)
                .animation(.easeInOut)
            }
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
        .environmentObject(appStore)
    }
}
