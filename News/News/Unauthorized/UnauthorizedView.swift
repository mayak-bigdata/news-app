//
//  UnauthorizedView.swift
//  News
//
//  Created by David Davitadze on 07.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import SwiftUI

//MARK: - Sign In Button
struct SignInButton: View {
    @EnvironmentObject var store: Store<AppState>
    @State var showAuth: Bool = false
    
    var body: some View {
        let showAuthBinding = Binding<Bool>(
            get: {
                self.showAuth && self.store.state.authState.authToken == nil
            },
            set: {
                self.showAuth = $0
            }
        )
        
        return Button(action: {
            self.showAuth.toggle()
        }) {
            Image(systemName: "person.crop.circle")
            .resizable()
            .aspectRatio(contentMode: .fit)
        }
        .frame(width: 44, height: 44)
//        .padding(.trailing, 10)
        .padding(.trailing)
        .sheet(isPresented: showAuthBinding) {
            LoginView()
            .environmentObject(appStore)
            .animation(.easeInOut)
        }
    }
}

// MARK: - Sign In Button Preview
struct SignInButton_Previews: PreviewProvider {
    static var previews: some View {
        SignInButton()
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}

// MARK: - Unauthorized Header View
struct UnauthorizedHeader: View {
    @EnvironmentObject var store: Store<AppState>
    
    var body: some View {
        HStack(alignment: .center) {
            Text("For You")
            .bold()
                .font(.largeTitle)
//            .font(.title)

            Spacer()
            
            SignInButton()
        }
    }
}

// MARK: - Unauthorized Header Preview
struct UnauthorizedHeader_Previews: PreviewProvider {
    static var previews: some View {
        UnauthorizedHeader()
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}

// MARK: - Unauthorized View
struct UnauthorizedView: View {
    @EnvironmentObject var store: Store<AppState>

    /// UI constants
//    let verticalMargin: CGFloat = 50
//    let titleMargin: CGFloat = 25

    var body: some View {
        CardBorderOverlay {
            VStack(alignment: .leading) {
                UnauthorizedHeader()
//                .padding(.bottom, self.titleMargin)
//                .padding(.top, self.verticalMargin)
                .padding(.vertical)

                Text("To listen to our hand picked personalized audio-digests please sign in")
                .font(.title)
//                .padding(.bottom, self.verticalMargin)

                Spacer()
            }
        }
    }
}

// MARK: - Unauthorized Preview
struct ForYouView_Previews: PreviewProvider {
    static var previews: some View {
        UnauthorizedView()
        .environmentObject(appStore)
        .previewLayout(.sizeThatFits)
    }
}

//    struct Props {
//
//    }

    // Maps state onto concrete view props
//    func map(state: AppState) -> Props {
//        return Props(showAuthScreen: state.authToken == nil && state.isAuthenticating)
//    }

//    struct ActionCreators {
//        let pressSignInButton: ActionCreator
//    }

    // Bind function
//    func bind(bind: Bind) -> ActionCreators {
//        return ActionCreators(pressSignInButton: bind(ForYouAction.pressedSignInButton))
//    }
