//
//  CardOverlayView.swift
//  News
//
//  Created by David Davitadze on 12.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import SwiftUI

struct CardBorderOverlay<V: View>: View {
    let contents: () -> V
    
    init(contents: @escaping () -> V) {
        self.contents = contents
    }
    
    var body: some View {
        self.contents()
//        .frame(width: UIScreen.main.bounds.size.width - cardViewWidthMargin,
//               height: UIScreen.main.bounds.size.height - cardViewHeightMargin)
        .padding()
        .background(Color.white)
        .cornerRadius(24)
        .overlay(
            RoundedRectangle(cornerRadius: 24)
                .stroke(Color.gray, lineWidth: 1)
        )
        .shadow(color: Color.gray, radius: 10)
    }
}

struct CardOverlayView_Previews: PreviewProvider {
    static var previews: some View {
        CardBorderOverlay() {
            ContentView()
        }
        .previewLayout(.sizeThatFits)
    }
}
