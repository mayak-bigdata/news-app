//
//  DraggableScrollviewOverlay.swift
//  News
//
//  Created by David Davitadze on 12.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import SwiftUI

struct DraggableScrollviewOverlay<V: View>: View {
    /// Pagination state
    @State private var offset: CGFloat = 0
    @State private var index = 0
    
    let numberOfPages: Int
    let spacing: CGFloat
    
    let contents: () -> V

    init(numberOfPages: Int, spacing: CGFloat, contents: @escaping () -> V) {
        self.numberOfPages = numberOfPages
        self.spacing = spacing
        self.contents = contents
    }
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.horizontal, showsIndicators: false) {
                self.contents()
            }
            .content.offset(x: self.offset)
            .frame(width: geometry.size.width, alignment: .leading)
            .gesture(
                DragGesture()
                    .onChanged({ value in
                        self.offset = value.translation.width - (geometry.size.width + self.spacing) * CGFloat(self.index)
                    })
                    .onEnded({ value in
                        if -value.predictedEndTranslation.width > geometry.size.width / 2,
                            self.index < self.numberOfPages - 1 {
                            self.index += 1
                        }
                        if value.predictedEndTranslation.width > geometry.size.width / 2,
                            self.index > 0 {
                            self.index -= 1
                        }
                        withAnimation { self.offset = -(geometry.size.width + self.spacing) * CGFloat(self.index) }
                    })
            )
        }
    }
}

struct DraggableScrollOverlay_Previews: PreviewProvider {
    static var previews: some View {
        DraggableScrollviewOverlay(numberOfPages: 1, spacing: 30) {
            ContentView()
        }
        .previewLayout(.sizeThatFits)
    }
}

struct VerticalScrollViewOverlay<V: View>: View {
    let contents: () -> V
    
    init(contents: @escaping () -> V) {
        self.contents = contents
    }
    
    var body: some View {
//        GeometryReader { geometry in
            ScrollView(.vertical, showsIndicators: false) {
                self.contents()
            }
//            .frame(height: geometry.size.height, alignment: .leading)
//        }
    }
}

struct VerticalScrollViewOverlay_Previews: PreviewProvider {
    static var previews: some View {
        VerticalScrollViewOverlay() {
            ContentView()
        }
        .previewLayout(.sizeThatFits)
    }
}
