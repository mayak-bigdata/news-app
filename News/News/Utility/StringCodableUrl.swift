//
//  StringCodableUrl.swift
//  News
//
//  Created by David Davitadze on 13.02.2020.
//  Copyright © 2020 David Davitadze. All rights reserved.
//

import Foundation

extension String{
    var encodeUrl : String
    {
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    var decodeUrl : String
    {
        return self.removingPercentEncoding!
    }
}
